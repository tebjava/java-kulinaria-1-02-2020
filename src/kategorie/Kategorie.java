package kategorie;

public class Kategorie {
	private String nazwa;
	private int kategoria; //0 - posi�ki, 1 - regiony
	
	public Kategorie() {
		
	}
	
	public Kategorie(String nazwa) {
		this(nazwa,0);
	}
	
	public Kategorie(String nazwa, int kategoria) {
		this(nazwa,(Object)kategoria);
	}
	
	public Kategorie(String nazwa, String kategoria) {
		this(nazwa,(Object)kategoria);
	}
	
	//niekoniecznie efektywne, acz "eleganckie" - wszystko w jednej metodzie-konstruktorze!
	private Kategorie(String nazwa, Object kategoria) {
		this.nazwa=nazwa;
		if (kategoria.getClass().isPrimitive()) {
			//potencja� kodu "na przysz�o�� - gdyby�my dodali wi�cej kategorii posi�k�w
			//ni� mamy obecnie
			if ((int)kategoria > 1 || (int)kategoria < 0)
				this.kategoria = -1; //nie mamy kategorii - nieustalona!
			else 
				this.kategoria = (int)kategoria;
		}
		else {
			switch((String)kategoria) {
			case "posi�ki":
			case "posi�ek":
			case "posilek":
			case "posilki": this.kategoria=0; break;
			case "region":
			case "regiony": 
			case "regionalne": this.kategoria=1;
			default: this.kategoria=-1;
			}
		}
	}
	
	public void ustawNazwa(String nazwa) {
		ustawNazwa(nazwa, 0);
	}
	
	public void ustawNazwa(String nazwa, int kategoria) {
		this.nazwa=nazwa;
		this.kategoria=kategoria;
	}
	
	public String pobierzNazwa() {
		return nazwa;
	}
	
	public int pobierzKategoria() {
		return (int)pobierzKategoria(false);
	}
	
	public Object pobierzKategoria(boolean nazwa) {
		if (!nazwa)
			return kategoria;
		switch(kategoria) {
			case 0: return "posi�ek";
			case 1: return "region kuchni";
		}
		return -1;
	}
}
