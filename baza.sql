-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 01 Lut 2020, 10:49
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `przepisy_java`
--
CREATE DATABASE IF NOT EXISTS `przepisy_java` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `przepisy_java`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategorie_posilkow`
--

DROP TABLE IF EXISTS `kategorie_posilkow`;
CREATE TABLE `kategorie_posilkow` (
  `id_kategoria_posilkow` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `kategorie_posilkow`
--

INSERT INTO `kategorie_posilkow` (`id_kategoria_posilkow`, `nazwa`) VALUES
(17, 'Drugie  danie'),
(22, 'Kolacja'),
(21, 'Obiad'),
(16, 'Pierwsze danie'),
(19, 'Sałatka'),
(20, 'Śniadanie'),
(18, 'Zupa');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kategorie_regionow`
--

DROP TABLE IF EXISTS `kategorie_regionow`;
CREATE TABLE `kategorie_regionow` (
  `id_kategoria_regionow` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `kategorie_regionow`
--

INSERT INTO `kategorie_regionow` (`id_kategoria_regionow`, `nazwa`) VALUES
(4, 'amerykańska'),
(2, 'polska'),
(1, 'śródziemnomorska'),
(5, 'tajska'),
(3, 'węgierska');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `multimedia`
--

DROP TABLE IF EXISTS `multimedia`;
CREATE TABLE `multimedia` (
  `id_multimedia` bigint(20) UNSIGNED NOT NULL,
  `sciezka` varchar(300) NOT NULL,
  `id_typ` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `multimedia`
--

INSERT INTO `multimedia` (`id_multimedia`, `sciezka`, `id_typ`) VALUES
(1, 'C:\\zdjecia\\0001.jpg', 1),
(2, 'C:\\zdjecia\\00023.jpg', 1),
(8, 'C:\\video\\0001.webm', 5),
(9, 'C:\\zdjecia\\0010.jpg', 2),
(10, 'C:\\zdjecia\\0020.png', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy`
--

DROP TABLE IF EXISTS `przepisy`;
CREATE TABLE `przepisy` (
  `id_przepis` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `czas_przygotowania` smallint(5) UNSIGNED NOT NULL,
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `przepisy`
--

INSERT INTO `przepisy` (`id_przepis`, `nazwa`, `czas_przygotowania`, `opis`) VALUES
(1, 'Gulasz', 45, ''),
(2, 'jajecznica', 15, '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy_kategorie_posilkow`
--

DROP TABLE IF EXISTS `przepisy_kategorie_posilkow`;
CREATE TABLE `przepisy_kategorie_posilkow` (
  `id_przepis` bigint(20) UNSIGNED NOT NULL,
  `id_kategorie_posilkow` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `przepisy_kategorie_posilkow`
--

INSERT INTO `przepisy_kategorie_posilkow` (`id_przepis`, `id_kategorie_posilkow`) VALUES
(2, 20),
(2, 22);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy_kategorie_regionalne`
--

DROP TABLE IF EXISTS `przepisy_kategorie_regionalne`;
CREATE TABLE `przepisy_kategorie_regionalne` (
  `id_przepis` bigint(20) UNSIGNED NOT NULL,
  `id_kategoria_regionow` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `przepisy_kategorie_regionalne`
--

INSERT INTO `przepisy_kategorie_regionalne` (`id_przepis`, `id_kategoria_regionow`) VALUES
(2, 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy_multimedia`
--

DROP TABLE IF EXISTS `przepisy_multimedia`;
CREATE TABLE `przepisy_multimedia` (
  `id_przepis` bigint(20) UNSIGNED NOT NULL,
  `id_multimedia` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `przepisy_multimedia`
--

INSERT INTO `przepisy_multimedia` (`id_przepis`, `id_multimedia`) VALUES
(2, 2),
(2, 10);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy_skladniki`
--

DROP TABLE IF EXISTS `przepisy_skladniki`;
CREATE TABLE `przepisy_skladniki` (
  `id_przepis` bigint(20) UNSIGNED NOT NULL,
  `id_skladnik` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `przepisy_skladniki`
--

INSERT INTO `przepisy_skladniki` (`id_przepis`, `id_skladnik`) VALUES
(2, 8),
(2, 9),
(2, 3);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `skladniki`
--

DROP TABLE IF EXISTS `skladniki`;
CREATE TABLE `skladniki` (
  `id_skladnik` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(30) NOT NULL,
  `jednostka_miary` varchar(10) NOT NULL DEFAULT 'gram',
  `kalorycznosc` int(7) UNSIGNED NOT NULL COMMENT 'To pole podawane w kcal (zakładając jedno jednostkę miary produktu)',
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `skladniki`
--

INSERT INTO `skladniki` (`id_skladnik`, `nazwa`, `jednostka_miary`, `kalorycznosc`, `opis`) VALUES
(1, 'pomidor', 'gram', 28, ''),
(2, 'woda', 'ml', 1, ''),
(3, 'sol', 'gram', 0, ''),
(4, 'gruszka', 'gram', 45, ''),
(5, 'wieprzowina', 'gram', 110, ''),
(6, 'polędwica', 'gram', 98, ''),
(7, 'marchew', 'gram', 33, ''),
(8, 'jajo', 'sztuka', 0, ''),
(9, 'bekon', 'gram', 120, '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `typy`
--

DROP TABLE IF EXISTS `typy`;
CREATE TABLE `typy` (
  `id_typ` bigint(20) UNSIGNED NOT NULL,
  `wartosc` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `typy`
--

INSERT INTO `typy` (`id_typ`, `wartosc`) VALUES
(8, 'document/pd'),
(9, 'document/pdf'),
(1, 'image/jpeg'),
(2, 'image/png'),
(5, 'image/webm'),
(6, 'video/avi'),
(7, 'video/mpeg');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `kategorie_posilkow`
--
ALTER TABLE `kategorie_posilkow`
  ADD UNIQUE KEY `id_kategoria_posilkow` (`id_kategoria_posilkow`),
  ADD UNIQUE KEY `nazwa` (`nazwa`);

--
-- Indeksy dla tabeli `kategorie_regionow`
--
ALTER TABLE `kategorie_regionow`
  ADD UNIQUE KEY `id_kategoria_regionow` (`id_kategoria_regionow`),
  ADD UNIQUE KEY `nazwa` (`nazwa`);

--
-- Indeksy dla tabeli `multimedia`
--
ALTER TABLE `multimedia`
  ADD UNIQUE KEY `id_multimedia` (`id_multimedia`),
  ADD KEY `id_typ` (`id_typ`);

--
-- Indeksy dla tabeli `przepisy`
--
ALTER TABLE `przepisy`
  ADD UNIQUE KEY `id_przepis` (`id_przepis`);

--
-- Indeksy dla tabeli `przepisy_kategorie_posilkow`
--
ALTER TABLE `przepisy_kategorie_posilkow`
  ADD KEY `id_przepis` (`id_przepis`),
  ADD KEY `id_kategorie_posilkow` (`id_kategorie_posilkow`);

--
-- Indeksy dla tabeli `przepisy_kategorie_regionalne`
--
ALTER TABLE `przepisy_kategorie_regionalne`
  ADD KEY `id_przepis` (`id_przepis`),
  ADD KEY `id_kategoria_regionow` (`id_kategoria_regionow`);

--
-- Indeksy dla tabeli `przepisy_multimedia`
--
ALTER TABLE `przepisy_multimedia`
  ADD KEY `id_przepis` (`id_przepis`),
  ADD KEY `id_multimedia` (`id_multimedia`);

--
-- Indeksy dla tabeli `przepisy_skladniki`
--
ALTER TABLE `przepisy_skladniki`
  ADD KEY `id_przepis` (`id_przepis`),
  ADD KEY `id_skladnik` (`id_skladnik`);

--
-- Indeksy dla tabeli `skladniki`
--
ALTER TABLE `skladniki`
  ADD UNIQUE KEY `id_skladnik` (`id_skladnik`);

--
-- Indeksy dla tabeli `typy`
--
ALTER TABLE `typy`
  ADD UNIQUE KEY `id_typ` (`id_typ`),
  ADD UNIQUE KEY `wartosc` (`wartosc`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `kategorie_posilkow`
--
ALTER TABLE `kategorie_posilkow`
  MODIFY `id_kategoria_posilkow` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT dla tabeli `kategorie_regionow`
--
ALTER TABLE `kategorie_regionow`
  MODIFY `id_kategoria_regionow` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT dla tabeli `multimedia`
--
ALTER TABLE `multimedia`
  MODIFY `id_multimedia` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `przepisy`
--
ALTER TABLE `przepisy`
  MODIFY `id_przepis` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `skladniki`
--
ALTER TABLE `skladniki`
  MODIFY `id_skladnik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT dla tabeli `typy`
--
ALTER TABLE `typy`
  MODIFY `id_typ` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `multimedia`
--
ALTER TABLE `multimedia`
  ADD CONSTRAINT `multimedia_ibfk_1` FOREIGN KEY (`id_typ`) REFERENCES `typy` (`id_typ`);

--
-- Ograniczenia dla tabeli `przepisy_kategorie_posilkow`
--
ALTER TABLE `przepisy_kategorie_posilkow`
  ADD CONSTRAINT `przepisy_kategorie_posilkow_ibfk_1` FOREIGN KEY (`id_przepis`) REFERENCES `przepisy` (`id_przepis`),
  ADD CONSTRAINT `przepisy_kategorie_posilkow_ibfk_2` FOREIGN KEY (`id_kategorie_posilkow`) REFERENCES `kategorie_posilkow` (`id_kategoria_posilkow`);

--
-- Ograniczenia dla tabeli `przepisy_kategorie_regionalne`
--
ALTER TABLE `przepisy_kategorie_regionalne`
  ADD CONSTRAINT `przepisy_kategorie_regionalne_ibfk_1` FOREIGN KEY (`id_kategoria_regionow`) REFERENCES `kategorie_regionow` (`id_kategoria_regionow`),
  ADD CONSTRAINT `przepisy_kategorie_regionalne_ibfk_2` FOREIGN KEY (`id_przepis`) REFERENCES `przepisy` (`id_przepis`);

--
-- Ograniczenia dla tabeli `przepisy_multimedia`
--
ALTER TABLE `przepisy_multimedia`
  ADD CONSTRAINT `przepisy_multimedia_ibfk_1` FOREIGN KEY (`id_przepis`) REFERENCES `przepisy` (`id_przepis`),
  ADD CONSTRAINT `przepisy_multimedia_ibfk_2` FOREIGN KEY (`id_multimedia`) REFERENCES `multimedia` (`id_multimedia`);

--
-- Ograniczenia dla tabeli `przepisy_skladniki`
--
ALTER TABLE `przepisy_skladniki`
  ADD CONSTRAINT `przepisy_skladniki_ibfk_1` FOREIGN KEY (`id_przepis`) REFERENCES `przepisy` (`id_przepis`),
  ADD CONSTRAINT `przepisy_skladniki_ibfk_2` FOREIGN KEY (`id_skladnik`) REFERENCES `skladniki` (`id_skladnik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
